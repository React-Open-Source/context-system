import React from 'react';
import {LanguageStore} from './modules/languages/Language-Context';
import Language from "./modules/languages/Language-Management";
import AppRouter from "./routing/AppRouter";
class App extends React.Component {



    render() {
        return (
            <div>
                <AppRouter />
            </div>
        );
    }

}

export default App;