import React from 'react';

const initialState = {
    language: 'english',
    name: 'afshin',
    family: 'imani',
    data: [1, 2, 3],
    obj: {id: '12', age: 236}
}

const LanguageContext = React.createContext({...initialState});


export class LanguageStore extends React.Component {

    state = {
        ...initialState
    }

    onChangeLanguage = async language => {
        await this.setState({language})
    }

    render() {
        // console.log('this.state',this.state);
        return (
            <LanguageContext.Provider value={{...this.state, onChangeLanguage: this.onChangeLanguage}}>
                {this.props.children}
            </LanguageContext.Provider>
        );
    }
}

export default LanguageContext;



