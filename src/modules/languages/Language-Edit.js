import React, {Component} from 'react';
import LanguageContext from "./Language-Context";

class LanguageEdit extends Component {

    render() {
        console.log('LanguageEdit', this.context);
        return (
            <div>
                Edit Language
            </div>
        );
    }

}

LanguageEdit.contextType = LanguageContext;

export default LanguageEdit;