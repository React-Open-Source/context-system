import React, {Component} from 'react';
import LanguageContext from "./Language-Context";

class Language extends Component {
    render() {
        console.log('this.context', this.context);
        return (
            <div>
                <h1>select lang</h1>
                <button onClick={() => this.context.onChangeLanguage('english')}>english</button>
                <button onClick={() => this.context.onChangeLanguage('france')}>france</button>
                <hr/>
                {this.context.language}
            </div>
        );
    }
}

Language.contextType = LanguageContext;

export default Language;