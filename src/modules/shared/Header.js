import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class Header extends Component {
    render() {
        return (
            <div>
                <Link to={'/'} >Home</Link> |
                <Link to={'/language'} >Language</Link> |
                <Link to={'/language/edit'} >Edit Language</Link>
            </div>
        );
    }
}

export default Header;