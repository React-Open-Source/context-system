import React from 'react';
import {Router, Switch, Route} from 'react-router-dom';
import Home from "../modules/home/Home";
import Language from "../modules/languages/Language-Management";
import {LanguageStore} from "../modules/languages/Language-Context";
import history from "./History";
import Header from "../modules/shared/Header";
import LanguageEdit from "../modules/languages/Language-Edit";

class AppRouter extends React.Component {

    render() {
        return (
            <Router history={history}>
                <Header />
                <Switch>
                    <Route exact path={'/'}   render={() => <Home />}/>
                    <Route exact path={'/language'} render={() => <LanguageStore><Language/></LanguageStore>}/>
                    <Route path={'/language/edit'} render={() => <LanguageStore><LanguageEdit/></LanguageStore>}/>
                </Switch>
            </Router>
        );
    }
}

export default AppRouter;